#include "dwm1001.h"

void app_main(){
    // Initialize the SPI bus on the ESP32 and both SPI devices (DWM units)
    init();
    dwm_resp_tag_loc_get loc1, loc2;
    dwm_resp_cfg_get cfg1, cfg2;
    dwm_resp_err err;
    // Selector for what DWM function to test: 
    // 0-Location, 1-Configuration, 2-SetTag&Reset, 3-SetAnchor&Reset, 4-Reset, 5-ConfigureDataReadyInterrupt
    uint8_t select = 1;

    // DWM function argument for Tag Configure 
    uint8_t tagcfg_arg[] = {0x98, 0x00};
    // DWM function argument for Anchor Configure 
    uint8_t anccfg_arg = 0x98;
    // DWM function argument for Interrupt Configure 
    uint8_t intcfg_arg = 0x02;


    while(1){  
        vTaskDelay(1000/ portTICK_RATE_MS);
        
        if(select == 0){
            loc1 = dwm_tag_loc_get(NODE_TYPE_TAG); 
            loc1 = dwm_tag_loc_get(NODE_TYPE_ANC); 
        }            
        else if(select == 1){
            cfg1 = dwm_cfg_get(NODE_TYPE_TAG);
            cfg2 = dwm_cfg_get(NODE_TYPE_ANC);
        }
        else if(select == 2){
            err = dwm_tag_set(tagcfg_arg, NODE_TYPE_TAG);
            err = dwm_rst(NODE_TYPE_TAG);
        }
        else if(select == 3){
            err = dwm_anc_set(anccfg_arg, NODE_TYPE_TAG);
            err = dwm_rst(NODE_TYPE_TAG);
        }
        else if(select == 4){
            err = dwm_rst(NODE_TYPE_TAG);
        }
        else if(select == 5){
            err = dwm_int_cfg(intcfg_arg, NODE_TYPE_ANC);
            vTaskDelay(1000/ portTICK_RATE_MS);
            err = dwm_int_cfg(intcfg_arg, NODE_TYPE_TAG);
        }
        printf("\n");
        vTaskDelay(10000/ portTICK_RATE_MS);
    }
}