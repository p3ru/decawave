/** @file */
#ifndef DWM1001_H_
#define DWM1001_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/spi_master.h"
#include "soc/gpio_struct.h"
#include "driver/gpio.h"
#include "nvs_flash.h"
#include "nvs.h"

/* ----- ESP32 SPI PIN DEFINES -----*/
#define PIN_NUM_MISO 19
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK  18
/* ----- TAG DWM PIN DEFINES -----*/
#define PIN_NUM_CS1   5
/* ----- ANC DWM PIN DEFINES -----*/
#define PIN_NUM_CS2   17

/* ----- NODE TYPE DEFINE -----*/
#define NODE_TYPE_TAG 0
#define NODE_TYPE_ANC 1

/* ----- DEBUG DEFINE -----*/
#define TLV_DEBUG
#define FILTERED_RESPONSE_DEBUG

/* ----- ESP32 NECESSARY SPI STRUCT DECLARATION -----*/
esp_err_t ret;
spi_device_handle_t spi1;
spi_device_handle_t spi2;

/* ------- NODE TYPE TRANSMIT -------*/
/* - This global is written to      -*/
/* - EVERYTIME the ESP talks to DWM -*/
static bool DWM_NUM; // 0 - NODE_TYPE_TAG, 1 - NODE_TYPE_ANC

/***************************************************************
 *                   SPI TRANSACTION STRUCTS                   *
 * ----------------------------------------------------------- *
 *  These are the hidden structs that are passed around        *
 *  the SPI TRANSACTION FUNCTIONS (i.e. structs that pass the  *
 *  raw, unflitered spi transaction data)                      *
 ***************************************************************/
/**
 * @struct _dwm_write_tlv
 * @brief Houses the SPI transaction that will be sent from the ESP32 to the DWM
 * @var dwm_write_tlv::type
 * Contains the DWM specific function ID
 * @var dwm_write_tlv::length
 * Contains the length (in bytes) of the value being written to the DWM
 * @var dwm_write_tlv::value
 * Contains a 16-byte fixed array that houses the written values.
 */
struct _dwm_write_tlv{
    uint8_t type;
    uint8_t length;
    uint8_t value[16]; 
};
typedef struct _dwm_write_tlv dwm_write_tlv;

/**
 * @struct _dwm_response
 * @brief Houses the response the DWM sends back to the ESP32
 * @var dwm_write_tlv::rxsize
 * Contains the size (in bytes) of the DWM response. Useful as different DWM functions return responses of varying length
 * @var dwm_write_tlv::response
 * Contains the DWM response
 */
struct _dwm_response{
    uint8_t rxsize;
    uint8_t response[256];
};
typedef struct _dwm_response dwm_response;

/***************************************************************
 *                SPI FILTERED RESPONSE STRUCTS                *
 * ----------------------------------------------------------- *
 *  These are the structs that will be passed around the mesh  *
 *  (i.e. structs that hold the filtered spi responses)        *
 ***************************************************************/
/**
 * @struct _dwm_resp_err
 * @brief Houses the error message attached with the DWM response; useful for debugging.
 * @var _dwm_resp_err::err_code
 * Contains the ID (byte) of the DWM error. Every response has an error id indicating any error.
 * @var _dwm_resp_err::err_string
 * Contains a useful error description related to the code.
 */
struct _dwm_resp_err{
    uint8_t err_code;
    char *err_string;
};
typedef struct _dwm_resp_err dwm_resp_err;

/**
 * @struct _dwm_resp_cfg_get
 * @brief Houses the response to the DWM configuration function call
 * @var _dwm_resp_cfg_get::err
 * Contains the error message of the response
 * @var _dwm_resp_cfg_get::mode
 * Indicated if the node is configured as an Anchor or a Tag
 * @var _dwm_resp_cfg_get::initiator
 * Indicates if the node is configured to be an Anchor Initiator
 * @var _dwm_resp_cfg_get::bridge
 * Indicates if the node is configured to be a network bridge
 * @var _dwm_resp_cfg_get::accel_en
 * Indicates if the node's accelerometer is enabled 
 * @var _dwm_resp_cfg_get::meas_mode
 * Indicates if the node is configured for Two Way Ranging (always set as 0)
 * @var _dwm_resp_cfg_get::low_power_en
 * Indicates if the node has low-power mode enabled
 * @var _dwm_resp_cfg_get::loc_engine_en
 * Indicates if the node has location engine enabled 
 * @var _dwm_resp_cfg_get::led_en
 * Indicates if the node has leds enabled (used on DWM1001-DEV board)
 * @var _dwm_resp_cfg_get::ble_en
 * Indicates if the node has bluetooth enabled
 * @var _dwm_resp_cfg_get::fw_update_en
 * Indicates if the node has firmware updates enabled
 * @var _dwm_resp_cfg_get::uwb_mode
 * Indicates the node's UWB operation mode: 0 -offline, 1 –passive, 2 –active
 */
struct _dwm_resp_cfg_get{
    dwm_resp_err err;
    uint8_t mode : 1;
    uint8_t initiator : 1;
    uint8_t bridge : 1;
    uint8_t accel_en : 1;
    uint8_t meas_mode : 2;
    uint8_t low_power_en : 1;
    uint8_t loc_engine_en : 1;
    uint8_t led_en : 1;
    uint8_t ble_en : 1;
    uint8_t fw_update_en : 1;
    uint8_t uwb_mode : 2;
};
typedef struct _dwm_resp_cfg_get dwm_resp_cfg_get;

/**
 * @struct _dwm_resp_tag_loc_get
 * @brief Houses the response to the DWM location function call
 * @var _dwm_resp_tag_loc_get::err
 * Contains the error message of the response
 * @var _dwm_resp_tag_loc_get::num_dists
 * Contains the number of distances recieved by the DWM
 * @var _dwm_resp_tag_loc_get::id
 * Contains the UWB ids of each of the DWM anchors ranged to 
 * @var _dwm_resp_tag_loc_get::dist
 * Contains the distances to each of the ranged DWM anchors
 * @var _dwm_resp_tag_loc_get::qf
 * Contains the quality factor of the reported distances
 */
struct _dwm_resp_tag_loc_get{
    dwm_resp_err err;
    uint8_t num_dists;
    uint16_t *id;
    uint32_t *dist; 
    uint8_t *qf;
};
typedef struct _dwm_resp_tag_loc_get dwm_resp_tag_loc_get;

/***************************************************************
 *                SPI INITIALIZATION FUNCTIONS                 *
 ***************************************************************/

/**
 * Initiates the SPI bus used by the ESP32 and configures the
 *  SPI devices (both on-board DWM modules) 
 */
void init();

/***************************************************************
 *                 SPI TRANSACTION FUNCTIONS                   *
 ***************************************************************/
/**
 * Initiates the SPI write transaction from the ESP to the DWM
 */
void spi_write(dwm_write_tlv *);
/**
 * Keeps the ESP32 suspended until the DWM is done processing the transaction 
 */
void spi_wait(dwm_response *);
/**
 * Retrieves the response from the DWM to the specified function call
 */
void spi_receive(dwm_response *);
/**
 * Aggregates the spi write, wait, and receive functions for use with either of the on-board DWM modules
 */
void dwm_tlv(dwm_write_tlv *, dwm_response *, bool);

/***************************************************************
 *                    DWM SPI API FUNCTIONS                    *
 ***************************************************************/
/**
 * Configures the selected DWM as a Tag 
 */
dwm_resp_err dwm_tag_set(uint8_t *, bool);
/**
 * Configures the selected DWM as an Anchor
 */
dwm_resp_err dwm_anc_set(uint8_t, bool);
/**
 * Resets the selected DWM module
 */
dwm_resp_err dwm_rst(bool);
/**
 * Returns the configuration of the slected unit
 */
dwm_resp_cfg_get dwm_cfg_get(bool);
/**
 * Returns all distances (up to four) seen by the on-board DWM module
 */
dwm_resp_tag_loc_get dwm_tag_loc_get(bool);
/**
 * Enables data-ready interrupts on the selected DWM unit. GPIO19 on the specified unit is toggled high once the unit is ready with a response.
 */
dwm_resp_err dwm_int_cfg(uint8_t, bool);

#endif // DWM1001_H_