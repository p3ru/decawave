#include "dwm1001.h"

/* ----- ESP32 NECESSARY SPI STRUCT INITIALIZATION -----*/
spi_bus_config_t buscfg={
    .miso_io_num=PIN_NUM_MISO,
    .mosi_io_num=PIN_NUM_MOSI,
    .sclk_io_num=PIN_NUM_CLK,
    .quadwp_io_num=-1,
    .quadhd_io_num=-1
};
spi_device_interface_config_t devcfg1={
    .clock_speed_hz= 50*1000, //1*1000*1000,            //Clock out at 1 MHz
    .mode=0,                                //SPI mode 0
    .spics_io_num=PIN_NUM_CS1,               //CS pin
    .queue_size=1,                          //We want to be able to queue 7 transactions at a time
};
spi_device_interface_config_t devcfg2={
    .clock_speed_hz= 50*1000, //1*1000*1000,            //Clock out at 1 MHz
    .mode=0,                                //SPI mode 0
    .spics_io_num=PIN_NUM_CS2,               //CS pin
    .queue_size=1,                          //We want to be able to queue 7 transactions at a time
};

/* ----- DWM RESPONSE ERROR CODE DECLARATION & INITIALIZATION -----*/
char *dwm_err_codes[6] = {
    "OK",
    "Unknown command or broken TLV frame",
    "Internal error",
    "Invalid Parameter",
    "Busy",
    "Incoherent SPI, debug please"};

/***************************************************************
 *                SPI INITIALIZATION FUNCTIONS                 *
 ***************************************************************/
void init(){    
    ret=spi_bus_initialize(VSPI_HOST, &buscfg, 1);  //Initialize the SPI bus
    ESP_ERROR_CHECK(ret);
    
    ret=spi_bus_add_device(VSPI_HOST, &devcfg1, &spi1);  //Attach the DWM TAG to the SPI bus
    ESP_ERROR_CHECK(ret);

    ret=spi_bus_add_device(VSPI_HOST, &devcfg2, &spi2);  //Attach the DWM ANC to the SPI bus
    ESP_ERROR_CHECK(ret);
}

/***************************************************************
 *                  SPI TRANSACTION FUNCTIONS                  *
 ***************************************************************/

void spi_write(dwm_write_tlv *tlv){
    uint8_t write_len=2+tlv->length; //Capture transaction array length (accounting for type & length)
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=8*write_len;                   //Len is in bytes, transaction length is in bits.
    t.rxlength = 8*write_len;
    t.tx_buffer=tlv;              //Data
    t.rx_buffer=tlv;
    
    if(DWM_NUM == NODE_TYPE_TAG)
        ret=spi_device_transmit(spi1, &t);  // Transmit to TAG!
    else
        ret=spi_device_transmit(spi2, &t);  // Transmit to ANC!
    assert(ret==ESP_OK);      
}

void spi_wait(dwm_response *tlv){
    uint8_t txdummy = 0xFF;
    tlv->rxsize = 0x00;
    
    while(tlv->rxsize == 0x00){
        esp_err_t ret;
        spi_transaction_t t;
        memset(&t, 0, sizeof(t));     //Zero out the transaction
        t.length=8;                   //Len is in bytes, transaction length is in bits.
        t.rxlength = 8;
        t.tx_buffer= &txdummy;        //Data
        t.rx_buffer= &tlv->rxsize;
        vTaskDelay(200/ portTICK_RATE_MS);

        if(DWM_NUM == NODE_TYPE_TAG)
            ret=spi_device_transmit(spi1, &t);  // Transmit to TAG!
        else
            ret=spi_device_transmit(spi2, &t);  // Transmit to ANC!
        assert(ret==ESP_OK);
    } 
}

void spi_receive(dwm_response *tlv){
    memset(tlv->response, 0xFF, tlv->rxsize); // Initialize dummy transaction with 0xFFs

    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=8*tlv->rxsize;                   //Len is in bytes, transaction length is in bits.
    t.rxlength = 8*tlv->rxsize;
    t.tx_buffer= tlv->response;              //Data
    t.rx_buffer= tlv->response;
    
    if(DWM_NUM == NODE_TYPE_TAG)
        ret=spi_device_transmit(spi1, &t);  // Transmit to TAG!
    else
        ret=spi_device_transmit(spi2, &t);  // Transmit to ANC!
    assert(ret==ESP_OK);
}

void dwm_tlv(dwm_write_tlv *tlv, dwm_response *response, bool node_type){
    DWM_NUM = (node_type == 0) ? 1 : 0; 
    spi_write(tlv);
    spi_wait(response);
    spi_receive(response);

#ifdef TLV_DEBUG
    printf("[DWM %d]\n", node_type);
    printf("Write Response: ");
    printf("%02X, ", tlv->type);
    printf("%02X \n", tlv->length);
    printf("Rxsize: %02X\n", response->rxsize);
    printf("Transaction Response: ");
    for(uint8_t i=0; i<response->rxsize; i++)
        printf("%02X, ", response->response[i]);
    printf("\n");
#endif // DEBUG
}

/***************************************************************
 *                    DWM SPI API FUNCTIONS                    *
 ***************************************************************/
dwm_resp_err dwm_tag_set(uint8_t *tagcfg, bool DWM_NUM){
    /* --- Decalre & Initialize transaction variables --- */
    dwm_write_tlv tlv = {0x05, 0x02, {tagcfg[0],tagcfg[1]}};
    dwm_response response;
    dwm_resp_err err;
    /* --- Do the transaction --- */
    dwm_tlv(&tlv, &response, DWM_NUM);
    /* --- Error check the TLV response --- */
    err.err_code = response.response[2]; // Retrieve error code of transaction
    if(err.err_code < 5)
        err.err_string = dwm_err_codes[err.err_code]; // Retrieve error string corresponding to the code
    else
        err.err_string = dwm_err_codes[5]; // Retrieve error string corresponding to the code
    /* --- Debug Info --- */
    #ifdef TLV_DEBUG
    if(err.err_code != 0x00){
        printf("[ERROR]: Oops, something happened when executing 'dwm_tag_set()'"); 
        printf("\n\t-ERROR CODE: %02X\n\t-ERROR INFO: %s\n",err.err_code,err.err_string);
        printf("---------------------------------------------------------------------\n");
    }
    #endif // DEBUG

    return err;
}

dwm_resp_err dwm_anc_set(uint8_t anccfg, bool DWM_NUM){
    /* --- Decalre & Initialize transaction variables --- */
    dwm_write_tlv tlv = {0x07, 0x01, {anccfg}};
    dwm_response response;
    dwm_resp_err err;
    /* --- Do the transaction --- */
    dwm_tlv(&tlv, &response, DWM_NUM);
    /* --- Error check the TLV response --- */
    err.err_code = response.response[2]; // Retrieve error code of transaction
    if(err.err_code < 5)
        err.err_string = dwm_err_codes[err.err_code]; // Retrieve error string corresponding to the code
    else
        err.err_string = dwm_err_codes[5]; // Retrieve error string corresponding to the code
    /* --- Debug Info --- */
    #ifdef FILTERED_RESPONSE_DEBUG
    if(err.err_code != 0x00){
        printf("[ERROR]: Oops, something happened when executing 'dwm_anc_set()'"); 
        printf("\n\t-ERROR CODE: %02X\n\t-ERROR INFO: %s\n",err.err_code,err.err_string);
        printf("---------------------------------------------------------------------\n");
    }
    #endif // DEBUG
    
    return err;
}

dwm_resp_err dwm_rst(bool DWM_NUM){
    /* --- Decalre & Initialize transaction variables --- */
    dwm_write_tlv tlv = {0x14, 0x00, {0x00}};
    dwm_response response;
    dwm_resp_err err;
    /* --- Do the transaction --- */
    dwm_tlv(&tlv, &response, DWM_NUM);
    /* --- Error check the TLV response --- */
    err.err_code = response.response[2]; // Retrieve error code of transaction
    if(err.err_code < 5)
        err.err_string = dwm_err_codes[err.err_code]; // Retrieve error string corresponding to the code
    else
        err.err_string = dwm_err_codes[5]; // Retrieve error string corresponding to the code
    /* --- Debug Info --- */
    #ifdef FILTERED_RESPONSE_DEBUG
    if(err.err_code != 0x00){
        printf("[ERROR]: Oops, something happened when executing 'dwm_tag_set()'"); 
        printf("\n\t-ERROR CODE: %02X\n\t-ERROR INFO: %s\n",err.err_code,err.err_string);
        printf("---------------------------------------------------------------------\n");
    }
    #endif // DEBUG

    return err;
}

dwm_resp_cfg_get dwm_cfg_get(bool DWM_NUM){
    /* --- Decalre & Initialize transaction variables --- */
    dwm_write_tlv tlv = {0x08, 0x00, {0x00}};
    dwm_response response;
    dwm_resp_cfg_get cfg;
    /* --- Do the transaction --- */
    dwm_tlv(&tlv, &response, DWM_NUM);
    /* --- Error check the TLV response --- */
    cfg.err.err_code = response.response[2]; // Retrieve error code of transaction
    if(cfg.err.err_code < 5)
        cfg.err.err_string = dwm_err_codes[cfg.err.err_code]; // Retrieve error string corresponding to the code
    else
        cfg.err.err_string = dwm_err_codes[5]; // Retrieve error string corresponding to the code
    /* --- Extract Config Info--- */ 
    cfg.low_power_en = (response.response[5] >> 7) & 1; 
    cfg.loc_engine_en = (response.response[5] >> 6) & 1; 
    cfg.led_en = (response.response[5] >> 4) & 1; 
    cfg.ble_en = (response.response[5] >> 3) & 1; 
    cfg.fw_update_en = (response.response[5] >> 2) & 1;
    cfg.uwb_mode = response.response[5] & 1;
    cfg.mode = (response.response[6] >> 5) & 1; 
    cfg.initiator = (response.response[6] >> 4) & 1; 
    cfg.bridge = (response.response[6] >> 3) & 1; 
    cfg.accel_en = (response.response[6] >> 2) & 1; 
    cfg.meas_mode = response.response[6] & 1;
    /* --- Debug Info --- */
    #ifdef FILTERED_RESPONSE_DEBUG
    if(cfg.err.err_code == 0x00){
        printf("\nDWM Configuration: [DWM %d]\n", DWM_NUM);
        printf("\tlow_power_en - %d\n", cfg.low_power_en);
        printf("\tloc_engine_en - %d\n", cfg.loc_engine_en);
        printf("\tled_en - %d\n", cfg.led_en);
        printf("\tble_en - %d\n", cfg.ble_en);
        printf("\tfw_update_en - %d\n", cfg.fw_update_en);
        printf("\tuwb_mode - %d\t(0 - offline, 1 - passive, 2 - active)\n", cfg.uwb_mode);
        printf("\tmode - %d\t(0 - tag, 1 - anchor)\n", cfg.mode);
        printf("\tinitiator - %d\n", cfg.initiator);
        printf("\tbridge - %d\n", cfg.bridge);
        printf("\taccel_en - %d\n", cfg.accel_en);
        printf("\tmeas_mode - %d\t(0 - TWR, 1|2|3 - reserved)\n", cfg.meas_mode);
        printf("---------------------------------------------------------------------\n");
    } else  {
        printf("[DWM %d][ERROR]: Oops, something happened when executing 'dwm_cfg_get()'", DWM_NUM); 
        printf("\n\t-ERROR CODE: %02X\n\t-ERROR INFO: %s\n",cfg.err.err_code,cfg.err.err_string);
        printf("---------------------------------------------------------------------\n");

    }
    #endif // DEBUG

    return cfg;
}

dwm_resp_tag_loc_get dwm_tag_loc_get(bool DWM_NUM){
    /* --- Decalre & Initialize transaction variables --- */
    dwm_write_tlv tlv = {0x0C, 0x00, {0x00}};
    dwm_response response;
    dwm_resp_tag_loc_get loc;
    /* --- Do the transaction --- */
    dwm_tlv(&tlv, &response, DWM_NUM);
    /* --- Error check the TLV response --- */
    loc.err.err_code = response.response[2]; // Retrieve error code of transaction
    if(loc.err.err_code < 5)
        loc.err.err_string = dwm_err_codes[loc.err.err_code]; // Retrieve error string corresponding to the code
    else
        loc.err.err_string = dwm_err_codes[5]; // Retrieve error string corresponding to the code
    /* --- Grab # of distances seen by Tag --- */    
    loc.num_dists = response.response[20];  // Retrieve # of distances seen by node
    /* --- Allocate # slots of memory for each distance seen --- */    
    loc.id = (uint16_t*)malloc(sizeof(uint16_t)*loc.num_dists); // Allocate # of slots for distance id's
    loc.dist = (uint32_t*)malloc(sizeof(uint32_t)*loc.num_dists);   // Allocate # of slots for distances
    loc.qf = (uint8_t*)malloc(sizeof(uint8_t)*loc.num_dists);   // Allocate # of slots for quality factor of distances
    /* --- Populate the distances, their ids, & quality factor  --- */
    if((response.response[18] == 0x49) && (loc.err.err_code == 0x00)){  // If Node is a Tag
        for(uint8_t i=0; i<loc.num_dists; i++){
            // Retrieve the id from the response (little endian)
            loc.id[i] = response.response[21+(20*i)] | (response.response[22+(20*i)] << 8); 
            // Retrieve the dist from the response (little endian)
            loc.dist[i] = response.response[23+(20*i)] | (response.response[24+(20*i)] << 8) | (response.response[25+(20*i)] << 16) | (response.response[26+(20*i)] << 24); 
            // Retrieve the qf from the response (little endian)
            loc.qf[i] = response.response[27+(20*i)]; 
        }        
    }
    /* --- Debug Info --- */
    #ifdef FILTERED_RESPONSE_DEBUG
    if(loc.err.err_code == 0x00){
        printf("\n# of distances: %d\n",loc.num_dists);
        for(uint8_t i=0; i<loc.num_dists; i++){
            printf("- Dist %d:\n\tID: %d\n\tDISTANCE: %d mm\n\tQUALITY FACTOR: %d\n",(i+1),loc.id[i],loc.dist[i],loc.qf[i]);
        }
        printf("---------------------------------------------------------------------\n");
    } else  {
        printf("[ERROR]: Oops, something happened when executing 'dwm_tag_loc_get()'"); 
        printf("\n\t-ERROR CODE: %02X\n\t-ERROR INFO: %s\n",loc.err.err_code,loc.err.err_string);
        printf("---------------------------------------------------------------------\n");
    }
    #endif // DEBUG

    return loc;
}

dwm_resp_err dwm_int_cfg(uint8_t intcfg, bool DWM_NUM){
    /* --- Decalre & Initialize transaction variables --- */
    dwm_write_tlv tlv = {0x34, 0x01, {intcfg}};
    dwm_response response;
    dwm_resp_err err;
    /* --- Do the transaction --- */
    dwm_tlv(&tlv, &response, DWM_NUM);
    /* --- Error check the TLV response --- */
    err.err_code = response.response[2]; // Retrieve error code of transaction
    if(err.err_code < 5)
        err.err_string = dwm_err_codes[err.err_code]; // Retrieve error string corresponding to the code
    else
        err.err_string = dwm_err_codes[5]; // Retrieve error string corresponding to the code
    /* --- Debug Info --- */
    #ifdef FILTERED_RESPONSE_DEBUG
    if(err.err_code != 0x00){
        printf("[ERROR]: Oops, something happened when executing 'dwm_int_cfg()'"); 
        printf("\n\t-ERROR CODE: %02X\n\t-ERROR INFO: %s\n",err.err_code,err.err_string);
        printf("---------------------------------------------------------------------\n");
    }
    #endif // DEBUG
    
    return err;
}
